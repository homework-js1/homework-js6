
function getUserName() {
    let userName = prompt("Enter your name:");
    while (!(userName && /^[a-zA-Z]+$/.test(userName))) {
        userName = prompt("Oops...Re-enter your name: ");
    }
    return userName;
}
function getUserLastName() {
    let userLastName = prompt("Enter your last name:");
    while (!(userLastName && /^[a-zA-Z]+$/.test(userLastName))) {
        userLastName = prompt("Oops...Re-enter your last name: ");
    }
    return userLastName;
}
function input(){
    do{
        userBirthday = prompt("Enter your date of birth (template: dd.mm.yyyy):");
    }while(!(/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/.test(userBirthday)) || !checkDate(userBirthday.split('.')))

    return getUserBirthday(userBirthday.split('.'))
}

let birthday = input();




function checkDate(str){
    // month
    if ((str[1]) > 12 ){return false}
    //year
    if (str[2] < 1960 || str[2] > new Date().getFullYear().toString()){return false}
    //day
    let daysInMonth = ["31", "28", "31", "30", "31", "30", "31", "31"
        , "30", "31", "30", "31"];

    if ((0 === Number(str[2]) % 4 ) && (0 !== Number(str[2] & 100)) || (0 === Number(str[2]) % 400)){
        daysInMonth[1] = `${Number(daysInMonth[1]) + 1}`
    }

    if (str[0] > daysInMonth[Number(str[1])-1] || str[0] < 1){return false}

    return true
}

function getUserBirthday(str) {
    let dt = new Date(str[2], str[1]-1, str[0])
    let years = new Date().getFullYear() - dt.getFullYear();

    if (dt.getMonth() > new Date().getMonth())
    {
        years -= 1;
    }
    else if(dt.getMonth() === new Date().getMonth() && dt.getDay() > new Date().getDay()){
        years -= 1;
    }

    return years;
}
let year = userBirthday.slice(6);




function createNewUser(firstName, lastName, hbirthday) {
    const newUser = Object.create( {
            getLogin() {
                return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            },
            getAge() {
                return hbirthday;
             },
            getPassword() {
                return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + year ;
            }
        },
        {
            firstName: {
                value: firstName,
                writable: false
            },

            lastName: {
                value: lastName,
                writable: false
            },

            userAge: {
                value: hbirthday,
                writable: false


            }
        }

    )

    console.log(newUser.getLogin(), newUser.getAge(), newUser.getPassword());
    return newUser;

}
let user = createNewUser(getUserName(), getUserLastName(), birthday);

console.log(user);
